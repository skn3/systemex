	format	MS COFF
	extrn	_GetAsyncKeyState@4
	extrn	_GetCursorPos@4
	extrn	_GetTempPathW@8
	extrn	_GetVersionExW@4
	extrn	___bb_blitz_blitz
	extrn	___bb_system_system
	extrn	_bbArrayNew1D
	extrn	_bbArraySlice
	extrn	_bbEmptyArray
	extrn	_bbEmptyString
	extrn	_bbNullObject
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_bbStringClass
	extrn	_bbStringConcat
	extrn	_bbStringFind
	extrn	_bbStringFindLast
	extrn	_bbStringFromChar
	extrn	_bbStringReplace
	extrn	_bbStringSlice
	extrn	_bbStringSplit
	extrn	_bbStringToLower
	extrn	_brl_bank_CreateBank
	extrn	_brl_bank_PeekInt
	extrn	_brl_bank_PokeInt
	extrn	_brl_blitz_ArrayBoundsError
	extrn	_brl_blitz_NullObjectError
	extrn	_brl_system_Driver
	extrn	_skn3_requestFile
	public	___bb_systemex_systemex
	public	_skn3_systemex_GetMousePosition
	public	_skn3_systemex_GetOsVersion
	public	_skn3_systemex_GetTempDirectory
	public	_skn3_systemex_IsKeyDown
	public	_skn3_systemex_KeepSystemAlive
	public	_skn3_systemex_RequestFiles
	public	_skn3_systemex_TranslateKey
	section	"code" code
___bb_systemex_systemex:
	push	ebp
	mov	ebp,esp
	push	ebx
	cmp	dword [_494],0
	je	_495
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_495:
	mov	dword [_494],1
	push	ebp
	push	_477
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_system_system
	push	_47
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_51]
	and	eax,1
	cmp	eax,0
	jne	_52
	push	256
	push	_49
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [_50],eax
	or	dword [_51],1
_52:
	push	_53
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_55
	call	_brl_blitz_ArrayBoundsError
_55:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],1
	push	_57
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,2
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_59
	call	_brl_blitz_ArrayBoundsError
_59:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],2
	push	_61
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,3
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_63
	call	_brl_blitz_ArrayBoundsError
_63:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],1
	push	_65
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,8
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_67
	call	_brl_blitz_ArrayBoundsError
_67:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],1
	push	_69
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,9
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_71
	call	_brl_blitz_ArrayBoundsError
_71:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],9
	push	_73
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,12
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_75
	call	_brl_blitz_ArrayBoundsError
_75:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],12
	push	_77
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,13
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_79
	call	_brl_blitz_ArrayBoundsError
_79:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],13
	push	_81
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,13
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_83
	call	_brl_blitz_ArrayBoundsError
_83:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],156
	push	_85
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,27
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_87
	call	_brl_blitz_ArrayBoundsError
_87:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],27
	push	_89
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,32
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_91
	call	_brl_blitz_ArrayBoundsError
_91:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],32
	push	_93
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,33
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_95
	call	_brl_blitz_ArrayBoundsError
_95:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],33
	push	_97
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,34
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_99
	call	_brl_blitz_ArrayBoundsError
_99:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],34
	push	_101
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,35
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_103
	call	_brl_blitz_ArrayBoundsError
_103:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],35
	push	_105
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,36
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_107
	call	_brl_blitz_ArrayBoundsError
_107:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],36
	push	_109
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,37
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_111
	call	_brl_blitz_ArrayBoundsError
_111:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],37
	push	_113
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,38
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_115
	call	_brl_blitz_ArrayBoundsError
_115:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],38
	push	_117
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,39
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_119
	call	_brl_blitz_ArrayBoundsError
_119:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],39
	push	_121
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,40
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_123
	call	_brl_blitz_ArrayBoundsError
_123:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],40
	push	_125
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,41
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_127
	call	_brl_blitz_ArrayBoundsError
_127:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],41
	push	_129
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,42
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_131
	call	_brl_blitz_ArrayBoundsError
_131:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],42
	push	_133
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,43
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_135
	call	_brl_blitz_ArrayBoundsError
_135:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],43
	push	_137
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,44
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_139
	call	_brl_blitz_ArrayBoundsError
_139:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],44
	push	_141
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,45
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_143
	call	_brl_blitz_ArrayBoundsError
_143:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],45
	push	_145
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,46
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_147
	call	_brl_blitz_ArrayBoundsError
_147:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],46
	push	_149
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,48
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_151
	call	_brl_blitz_ArrayBoundsError
_151:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],48
	push	_153
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,49
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_155
	call	_brl_blitz_ArrayBoundsError
_155:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],49
	push	_157
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,50
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_159
	call	_brl_blitz_ArrayBoundsError
_159:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],50
	push	_161
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,51
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_163
	call	_brl_blitz_ArrayBoundsError
_163:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],51
	push	_165
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,52
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_167
	call	_brl_blitz_ArrayBoundsError
_167:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],52
	push	_169
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,53
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_171
	call	_brl_blitz_ArrayBoundsError
_171:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],53
	push	_173
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,54
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_175
	call	_brl_blitz_ArrayBoundsError
_175:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],54
	push	_177
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,55
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_179
	call	_brl_blitz_ArrayBoundsError
_179:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],55
	push	_181
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,56
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_183
	call	_brl_blitz_ArrayBoundsError
_183:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],56
	push	_185
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,57
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_187
	call	_brl_blitz_ArrayBoundsError
_187:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],57
	push	_189
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,65
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_191
	call	_brl_blitz_ArrayBoundsError
_191:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],65
	push	_193
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,66
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_195
	call	_brl_blitz_ArrayBoundsError
_195:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],66
	push	_197
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,67
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_199
	call	_brl_blitz_ArrayBoundsError
_199:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],67
	push	_201
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,68
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_203
	call	_brl_blitz_ArrayBoundsError
_203:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],68
	push	_205
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,69
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_207
	call	_brl_blitz_ArrayBoundsError
_207:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],69
	push	_209
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,70
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_211
	call	_brl_blitz_ArrayBoundsError
_211:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],70
	push	_213
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,71
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_215
	call	_brl_blitz_ArrayBoundsError
_215:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],71
	push	_217
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,72
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_219
	call	_brl_blitz_ArrayBoundsError
_219:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],72
	push	_221
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,73
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_223
	call	_brl_blitz_ArrayBoundsError
_223:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],73
	push	_225
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,74
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_227
	call	_brl_blitz_ArrayBoundsError
_227:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],74
	push	_229
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,75
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_231
	call	_brl_blitz_ArrayBoundsError
_231:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],75
	push	_233
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,76
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_235
	call	_brl_blitz_ArrayBoundsError
_235:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],76
	push	_237
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,77
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_239
	call	_brl_blitz_ArrayBoundsError
_239:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],77
	push	_241
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,78
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_243
	call	_brl_blitz_ArrayBoundsError
_243:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],78
	push	_245
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,79
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_247
	call	_brl_blitz_ArrayBoundsError
_247:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],79
	push	_249
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,80
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_251
	call	_brl_blitz_ArrayBoundsError
_251:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],80
	push	_253
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,81
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_255
	call	_brl_blitz_ArrayBoundsError
_255:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],81
	push	_257
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,82
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_259
	call	_brl_blitz_ArrayBoundsError
_259:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],82
	push	_261
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,83
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_263
	call	_brl_blitz_ArrayBoundsError
_263:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],83
	push	_265
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,84
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_267
	call	_brl_blitz_ArrayBoundsError
_267:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],84
	push	_269
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,85
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_271
	call	_brl_blitz_ArrayBoundsError
_271:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],85
	push	_273
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,86
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_275
	call	_brl_blitz_ArrayBoundsError
_275:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],86
	push	_277
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,87
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_279
	call	_brl_blitz_ArrayBoundsError
_279:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],87
	push	_281
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,88
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_283
	call	_brl_blitz_ArrayBoundsError
_283:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],88
	push	_285
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,89
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_287
	call	_brl_blitz_ArrayBoundsError
_287:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],89
	push	_289
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,90
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_291
	call	_brl_blitz_ArrayBoundsError
_291:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],90
	push	_293
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,96
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_295
	call	_brl_blitz_ArrayBoundsError
_295:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],96
	push	_297
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,97
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_299
	call	_brl_blitz_ArrayBoundsError
_299:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],97
	push	_301
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,98
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_303
	call	_brl_blitz_ArrayBoundsError
_303:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],98
	push	_305
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,99
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_307
	call	_brl_blitz_ArrayBoundsError
_307:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],99
	push	_309
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,100
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_311
	call	_brl_blitz_ArrayBoundsError
_311:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],100
	push	_313
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,101
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_315
	call	_brl_blitz_ArrayBoundsError
_315:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],101
	push	_317
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,102
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_319
	call	_brl_blitz_ArrayBoundsError
_319:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],102
	push	_321
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,103
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_323
	call	_brl_blitz_ArrayBoundsError
_323:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],103
	push	_325
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,104
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_327
	call	_brl_blitz_ArrayBoundsError
_327:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],104
	push	_329
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,105
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_331
	call	_brl_blitz_ArrayBoundsError
_331:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],105
	push	_333
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,106
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_335
	call	_brl_blitz_ArrayBoundsError
_335:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],106
	push	_337
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,107
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_339
	call	_brl_blitz_ArrayBoundsError
_339:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],107
	push	_341
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,109
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_343
	call	_brl_blitz_ArrayBoundsError
_343:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],109
	push	_345
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,110
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_347
	call	_brl_blitz_ArrayBoundsError
_347:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],110
	push	_349
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,111
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_351
	call	_brl_blitz_ArrayBoundsError
_351:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],111
	push	_353
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,112
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_355
	call	_brl_blitz_ArrayBoundsError
_355:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],112
	push	_357
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,113
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_359
	call	_brl_blitz_ArrayBoundsError
_359:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],113
	push	_361
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,114
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_363
	call	_brl_blitz_ArrayBoundsError
_363:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],114
	push	_365
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,115
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_367
	call	_brl_blitz_ArrayBoundsError
_367:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],115
	push	_369
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,116
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_371
	call	_brl_blitz_ArrayBoundsError
_371:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],116
	push	_373
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,117
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_375
	call	_brl_blitz_ArrayBoundsError
_375:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],117
	push	_377
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,118
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_379
	call	_brl_blitz_ArrayBoundsError
_379:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],118
	push	_381
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,119
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_383
	call	_brl_blitz_ArrayBoundsError
_383:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],119
	push	_385
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,120
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_387
	call	_brl_blitz_ArrayBoundsError
_387:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],120
	push	_389
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,121
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_391
	call	_brl_blitz_ArrayBoundsError
_391:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],121
	push	_393
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,122
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_395
	call	_brl_blitz_ArrayBoundsError
_395:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],122
	push	_397
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,123
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_399
	call	_brl_blitz_ArrayBoundsError
_399:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],123
	push	_401
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,192
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_403
	call	_brl_blitz_ArrayBoundsError
_403:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],192
	push	_405
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,189
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_407
	call	_brl_blitz_ArrayBoundsError
_407:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],189
	push	_409
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,187
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_411
	call	_brl_blitz_ArrayBoundsError
_411:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],187
	push	_413
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,219
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_415
	call	_brl_blitz_ArrayBoundsError
_415:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],57
	push	_417
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,221
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_419
	call	_brl_blitz_ArrayBoundsError
_419:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],58
	push	_421
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,226
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_423
	call	_brl_blitz_ArrayBoundsError
_423:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],220
	push	_425
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,186
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_427
	call	_brl_blitz_ArrayBoundsError
_427:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],186
	push	_429
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,222
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_431
	call	_brl_blitz_ArrayBoundsError
_431:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],222
	push	_433
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,188
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_435
	call	_brl_blitz_ArrayBoundsError
_435:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],188
	push	_437
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,190
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_439
	call	_brl_blitz_ArrayBoundsError
_439:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],190
	push	_441
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,191
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_443
	call	_brl_blitz_ArrayBoundsError
_443:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],191
	push	_445
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,160
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_447
	call	_brl_blitz_ArrayBoundsError
_447:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],160
	push	_449
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,161
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_451
	call	_brl_blitz_ArrayBoundsError
_451:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],161
	push	_453
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,162
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_455
	call	_brl_blitz_ArrayBoundsError
_455:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],162
	push	_457
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,163
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_459
	call	_brl_blitz_ArrayBoundsError
_459:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],163
	push	_461
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,164
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_463
	call	_brl_blitz_ArrayBoundsError
_463:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],164
	push	_465
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,165
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_467
	call	_brl_blitz_ArrayBoundsError
_467:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],165
	push	_469
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,91
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_471
	call	_brl_blitz_ArrayBoundsError
_471:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],91
	push	_473
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,92
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_475
	call	_brl_blitz_ArrayBoundsError
_475:
	mov	eax,dword [_50]
	shl	ebx,2
	add	eax,ebx
	mov	dword [eax+24],92
	mov	ebx,0
	jmp	_23
_23:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_systemex_RequestFiles:
	push	ebp
	mov	ebp,esp
	sub	esp,56
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	dword [ebp-24],_bbEmptyString
	mov	dword [ebp-28],_bbEmptyString
	mov	dword [ebp-32],0
	mov	dword [ebp-36],_bbEmptyString
	mov	dword [ebp-40],0
	mov	dword [ebp-44],0
	mov	dword [ebp-48],0
	mov	dword [ebp-56],_bbEmptyString
	mov	dword [ebp-52],_bbEmptyArray
	mov	eax,ebp
	push	eax
	push	_578
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_496
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbEmptyString
	mov	dword [ebp-28],_bbEmptyString
	push	_499
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_3
	push	_2
	push	dword [ebp-20]
	call	_bbStringReplace
	add	esp,12
	mov	dword [ebp-20],eax
	push	_500
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_3
	push	dword [ebp-20]
	call	_bbStringFindLast
	add	esp,12
	mov	dword [ebp-32],eax
	push	_502
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-32],-1
	je	_503
	mov	eax,ebp
	push	eax
	push	_506
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_504
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-32]
	push	0
	push	dword [ebp-20]
	call	_bbStringSlice
	add	esp,12
	mov	dword [ebp-28],eax
	push	_505
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-20]
	push	dword [eax+8]
	mov	eax,dword [ebp-32]
	add	eax,1
	push	eax
	push	dword [ebp-20]
	call	_bbStringSlice
	add	esp,12
	mov	dword [ebp-24],eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_507
_503:
	mov	eax,ebp
	push	eax
	push	_509
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_508
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-20]
	mov	dword [ebp-24],eax
	call	dword [_bbOnDebugLeaveScope]
_507:
	push	_510
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-36],_bbEmptyString
	mov	dword [ebp-40],0
	mov	dword [ebp-44],0
	mov	dword [ebp-48],0
	push	_515
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_4
	push	dword [ebp-20]
	call	_bbStringFind
	add	esp,12
	mov	dword [ebp-44],eax
	push	_516
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-44],-1
	jle	_517
	mov	eax,ebp
	push	eax
	push	_542
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_518
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_5
	mov	eax,dword [ebp-20]
	push	dword [eax+8]
	mov	eax,dword [ebp-44]
	add	eax,1
	push	eax
	push	dword [ebp-20]
	call	_bbStringSlice
	add	esp,12
	push	eax
	call	_bbStringToLower
	add	esp,4
	push	eax
	push	_5
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-36],eax
	push	_519
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_bbStringToLower
	add	esp,4
	mov	dword [ebp-56],eax
	push	_521
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_7
	push	_6
	push	dword [ebp-56]
	call	_bbStringReplace
	add	esp,12
	mov	dword [ebp-56],eax
	push	_522
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_9
	push	_8
	push	dword [ebp-56]
	call	_bbStringReplace
	add	esp,12
	mov	dword [ebp-56],eax
	push	_523
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-36]
	push	dword [ebp-56]
	call	_bbStringFind
	add	esp,12
	mov	dword [ebp-44],eax
	push	_524
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-44],-1
	jle	_525
	mov	eax,ebp
	push	eax
	push	_541
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_526
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-48],-1
	push	_527
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-40],1
	push	_528
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_10
_12:
	mov	eax,ebp
	push	eax
	push	_540
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_529
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-48]
	add	eax,1
	push	eax
	push	_8
	push	dword [ebp-56]
	call	_bbStringFind
	add	esp,12
	mov	dword [ebp-48],eax
	push	_530
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-44]
	cmp	dword [ebp-48],eax
	jle	_531
	mov	eax,ebp
	push	eax
	push	_533
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_532
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_11
_531:
	push	_534
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-48],-1
	jne	_535
	mov	eax,ebp
	push	eax
	push	_538
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_536
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-40],0
	push	_537
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_11
_535:
	push	_539
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-40],1
	call	dword [_bbOnDebugLeaveScope]
_10:
	mov	eax,1
	cmp	eax,0
	jne	_12
_11:
	call	dword [_bbOnDebugLeaveScope]
_525:
	call	dword [_bbOnDebugLeaveScope]
_517:
	push	_545
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	cmp	dword [eax+8],0
	je	_546
	mov	eax,ebp
	push	eax
	push	_556
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_547
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_6
	push	dword [ebp-8]
	call	_bbStringFind
	add	esp,12
	cmp	eax,-1
	jne	_548
	mov	eax,ebp
	push	eax
	push	_550
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_549
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	push	_13
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
	jmp	_551
_548:
	mov	eax,ebp
	push	eax
	push	_553
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_552
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_14
	push	_6
	push	dword [ebp-8]
	call	_bbStringReplace
	add	esp,12
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_551:
	push	_554
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_15
	push	_8
	push	dword [ebp-8]
	call	_bbStringReplace
	add	esp,12
	mov	dword [ebp-8],eax
	push	_555
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_15
	push	_16
	push	_5
	push	dword [ebp-8]
	call	_bbStringReplace
	add	esp,12
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_546:
	push	_557
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_8
	push	dword [ebp-28]
	push	dword [ebp-24]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-40]
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	_skn3_requestFile
	add	esp,28
	push	eax
	call	_bbStringSplit
	add	esp,8
	mov	dword [ebp-52],eax
	push	_559
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-52]
	mov	eax,dword [eax+20]
	sub	eax,1
	push	eax
	push	0
	push	dword [ebp-52]
	push	_544
	call	_bbArraySlice
	add	esp,16
	mov	dword [ebp-52],eax
	push	_560
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-52]
	cmp	dword [eax+20],1
	jle	_561
	mov	eax,ebp
	push	eax
	push	_576
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_562
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	mov	eax,dword [ebp-52]
	cmp	ebx,dword [eax+20]
	jb	_564
	call	_brl_blitz_ArrayBoundsError
_564:
	mov	eax,dword [ebp-52]
	mov	eax,dword [eax+ebx*4+24]
	mov	dword [ebp-28],eax
	push	_565
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-52]
	push	dword [eax+20]
	push	1
	push	dword [ebp-52]
	push	_544
	call	_bbArraySlice
	add	esp,16
	mov	dword [ebp-52],eax
	push	_566
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-44],0
	mov	eax,dword [ebp-52]
	mov	edi,dword [eax+20]
	jmp	_567
_19:
	mov	eax,ebp
	push	eax
	push	_575
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_569
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-44]
	mov	eax,dword [ebp-52]
	cmp	ebx,dword [eax+20]
	jb	_571
	call	_brl_blitz_ArrayBoundsError
_571:
	mov	eax,dword [ebp-52]
	shl	ebx,2
	add	eax,ebx
	mov	esi,eax
	mov	ebx,dword [ebp-44]
	mov	eax,dword [ebp-52]
	cmp	ebx,dword [eax+20]
	jb	_574
	call	_brl_blitz_ArrayBoundsError
_574:
	mov	eax,dword [ebp-52]
	push	dword [eax+ebx*4+24]
	push	_3
	push	dword [ebp-28]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [esi+24],eax
	call	dword [_bbOnDebugLeaveScope]
_17:
	add	dword [ebp-44],1
_567:
	cmp	dword [ebp-44],edi
	jl	_19
_18:
	call	dword [_bbOnDebugLeaveScope]
_561:
	push	_577
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-52]
	jmp	_30
_30:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_systemex_TranslateKey:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_601
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_593
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	cmp	eax,0
	setge	al
	movzx	eax,al
	cmp	eax,0
	jne	_594
	mov	edx,dword [ebp-4]
	mov	eax,dword [_50]
	cmp	edx,dword [eax+20]
	setl	al
	movzx	eax,al
_594:
	cmp	eax,0
	je	_596
	push	ebp
	push	_600
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_597
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	mov	eax,dword [_50]
	cmp	ebx,dword [eax+20]
	jb	_599
	call	_brl_blitz_ArrayBoundsError
_599:
	mov	eax,dword [_50]
	mov	ebx,dword [eax+ebx*4+24]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_33
_596:
	mov	ebx,0
	jmp	_33
_33:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_systemex_IsKeyDown:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_613
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_604
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_skn3_systemex_TranslateKey
	add	esp,4
	mov	dword [ebp-4],eax
	push	_605
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],0
	jne	_606
	push	ebp
	push	_608
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_607
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_36
_606:
	push	_609
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_GetAsyncKeyState@4
	and	eax,32768
	cmp	eax,0
	je	_610
	push	ebp
	push	_612
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_611
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_36
_610:
	mov	ebx,0
	jmp	_36
_36:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_systemex_GetOsVersion:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	mov	dword [ebp-4],_bbNullObject
	mov	dword [ebp-8],0
	mov	dword [ebp-12],0
	push	ebp
	push	_667
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_615
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_617
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_616],-1
	jne	_618
	push	ebp
	push	_663
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_619
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_616],0
	push	_620
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	156
	call	_brl_bank_CreateBank
	add	esp,4
	mov	dword [ebp-4],eax
	push	_622
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	156
	push	0
	push	dword [ebp-4]
	call	_brl_bank_PokeInt
	add	esp,12
	push	_623
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_625
	call	_brl_blitz_NullObjectError
_625:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_GetVersionExW@4
	cmp	eax,0
	je	_626
	push	ebp
	push	_659
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_627
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	dword [ebp-4]
	call	_brl_bank_PeekInt
	add	esp,8
	mov	dword [ebp-8],eax
	push	_629
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	8
	push	dword [ebp-4]
	call	_brl_bank_PeekInt
	add	esp,8
	mov	dword [ebp-12],eax
	push	_631
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	cmp	eax,5
	je	_634
	cmp	eax,6
	je	_635
	jmp	_633
_634:
	push	ebp
	push	_648
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_636
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	eax,0
	je	_639
	cmp	eax,1
	je	_640
	cmp	eax,2
	je	_641
	jmp	_638
_639:
	push	ebp
	push	_643
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_642
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_616],1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_638
_640:
	push	ebp
	push	_645
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_644
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_616],2
	call	dword [_bbOnDebugLeaveScope]
	jmp	_638
_641:
	push	ebp
	push	_647
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_646
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_616],3
	call	dword [_bbOnDebugLeaveScope]
	jmp	_638
_638:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_633
_635:
	push	ebp
	push	_658
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_649
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	eax,0
	je	_652
	cmp	eax,1
	je	_653
	jmp	_651
_652:
	push	ebp
	push	_655
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_654
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_616],4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_651
_653:
	push	ebp
	push	_657
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_656
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_616],5
	call	dword [_bbOnDebugLeaveScope]
	jmp	_651
_651:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_633
_633:
	call	dword [_bbOnDebugLeaveScope]
_626:
	push	_662
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-4],_bbNullObject
	call	dword [_bbOnDebugLeaveScope]
_618:
	push	_666
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_616]
	jmp	_38
_38:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_systemex_GetTempDirectory:
	push	ebp
	mov	ebp,esp
	sub	esp,24
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	mov	dword [ebp-12],0
	mov	dword [ebp-16],0
	mov	dword [ebp-20],_bbNullObject
	mov	dword [ebp-24],0
	push	ebp
	push	_720
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_670
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_672
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_674
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_673]
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_675
	mov	eax,dword [ebp-4]
_675:
	cmp	eax,0
	je	_677
	push	ebp
	push	_713
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_678
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_673],1
	push	_679
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_671],_1
	push	_680
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],2
	push	_682
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],2048
	push	_684
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	imul	eax,dword [ebp-8]
	mov	dword [ebp-16],eax
	push	_686
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	_brl_bank_CreateBank
	add	esp,4
	mov	dword [ebp-20],eax
	push	_688
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_690
	call	_brl_blitz_NullObjectError
_690:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	push	dword [ebp-12]
	call	_GetTempPathW@8
	mov	dword [ebp-24],eax
	push	_692
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-16]
	sub	eax,dword [ebp-8]
	cmp	dword [ebp-24],eax
	jle	_693
	push	ebp
	push	_702
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_694
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-24]
	add	eax,dword [ebp-8]
	mov	dword [ebp-16],eax
	push	_695
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-24]
	cdq
	idiv	dword [ebp-8]
	mov	dword [ebp-12],eax
	push	_696
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_698
	call	_brl_blitz_NullObjectError
_698:
	mov	eax,dword [ebp-16]
	imul	eax,dword [ebp-8]
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+72]
	add	esp,8
	push	_699
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_701
	call	_brl_blitz_NullObjectError
_701:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	push	dword [ebp-16]
	call	_GetTempPathW@8
	mov	dword [ebp-24],eax
	call	dword [_bbOnDebugLeaveScope]
_693:
	push	_703
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-24],0
	jle	_704
	push	ebp
	push	_712
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_705
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	mov	ebx,dword [ebp-24]
	jmp	_706
_22:
	push	ebp
	push	_711
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_708
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_710
	call	_brl_blitz_NullObjectError
_710:
	mov	eax,dword [ebp-16]
	shl	eax,1
	push	eax
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+92]
	add	esp,8
	push	eax
	call	_bbStringFromChar
	add	esp,4
	push	eax
	push	dword [_671]
	call	_bbStringConcat
	add	esp,8
	mov	dword [_671],eax
	call	dword [_bbOnDebugLeaveScope]
_20:
	add	dword [ebp-16],1
_706:
	cmp	dword [ebp-16],ebx
	jl	_22
_21:
	call	dword [_bbOnDebugLeaveScope]
_704:
	call	dword [_bbOnDebugLeaveScope]
_677:
	push	_719
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_671]
	jmp	_41
_41:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_systemex_KeepSystemAlive:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_731
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_725
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_brl_system_Driver],_bbNullObject
	je	_726
	push	ebp
	push	_730
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_727
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_brl_system_Driver]
	cmp	ebx,_bbNullObject
	jne	_729
	call	_brl_blitz_NullObjectError
_729:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_726:
	mov	ebx,0
	jmp	_43
_43:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_skn3_systemex_GetMousePosition:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	dword [ebp-4],_bbEmptyArray
	push	ebp
	push	_738
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_733
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_734
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-4],eax
	push	_736
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	lea	eax,byte [eax+24]
	push	eax
	call	_GetCursorPos@4
	push	_737
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	jmp	_45
_45:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_494:
	dd	0
_478:
	db	"systemex",0
_479:
	db	"WINDOWS_UNKNOWN",0
_480:
	db	"i",0
	align	4
_481:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	48
_482:
	db	"WINDOWS_2000",0
	align	4
_483:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	49
_484:
	db	"WINDOWS_XP",0
	align	4
_485:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	50
_486:
	db	"WINDOWS_2003",0
	align	4
_487:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	51
_488:
	db	"WINDOWS_VISTA",0
	align	4
_489:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	52
_490:
	db	"WINDOWS_7",0
	align	4
_491:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	53
_492:
	db	"TranslateKeysMap",0
_493:
	db	"[]i",0
	align	4
_50:
	dd	_bbEmptyArray
	align	4
_477:
	dd	1
	dd	_478
	dd	1
	dd	_479
	dd	_480
	dd	_481
	dd	1
	dd	_482
	dd	_480
	dd	_483
	dd	1
	dd	_484
	dd	_480
	dd	_485
	dd	1
	dd	_486
	dd	_480
	dd	_487
	dd	1
	dd	_488
	dd	_480
	dd	_489
	dd	1
	dd	_490
	dd	_480
	dd	_491
	dd	4
	dd	_492
	dd	_493
	dd	_50
	dd	0
_48:
	db	"$BMXPATH/mod/skn3.mod/systemex.mod/systemex.bmx",0
	align	4
_47:
	dd	_48
	dd	51
	dd	1
	align	4
_51:
	dd	0
_49:
	db	"i",0
	align	4
_53:
	dd	_48
	dd	52
	dd	1
	align	4
_57:
	dd	_48
	dd	53
	dd	1
	align	4
_61:
	dd	_48
	dd	54
	dd	1
	align	4
_65:
	dd	_48
	dd	56
	dd	1
	align	4
_69:
	dd	_48
	dd	57
	dd	1
	align	4
_73:
	dd	_48
	dd	58
	dd	1
	align	4
_77:
	dd	_48
	dd	59
	dd	1
	align	4
_81:
	dd	_48
	dd	60
	dd	1
	align	4
_85:
	dd	_48
	dd	61
	dd	1
	align	4
_89:
	dd	_48
	dd	62
	dd	1
	align	4
_93:
	dd	_48
	dd	63
	dd	1
	align	4
_97:
	dd	_48
	dd	64
	dd	1
	align	4
_101:
	dd	_48
	dd	65
	dd	1
	align	4
_105:
	dd	_48
	dd	66
	dd	1
	align	4
_109:
	dd	_48
	dd	68
	dd	1
	align	4
_113:
	dd	_48
	dd	69
	dd	1
	align	4
_117:
	dd	_48
	dd	70
	dd	1
	align	4
_121:
	dd	_48
	dd	71
	dd	1
	align	4
_125:
	dd	_48
	dd	73
	dd	1
	align	4
_129:
	dd	_48
	dd	74
	dd	1
	align	4
_133:
	dd	_48
	dd	75
	dd	1
	align	4
_137:
	dd	_48
	dd	76
	dd	1
	align	4
_141:
	dd	_48
	dd	77
	dd	1
	align	4
_145:
	dd	_48
	dd	78
	dd	1
	align	4
_149:
	dd	_48
	dd	80
	dd	1
	align	4
_153:
	dd	_48
	dd	81
	dd	1
	align	4
_157:
	dd	_48
	dd	82
	dd	1
	align	4
_161:
	dd	_48
	dd	83
	dd	1
	align	4
_165:
	dd	_48
	dd	84
	dd	1
	align	4
_169:
	dd	_48
	dd	85
	dd	1
	align	4
_173:
	dd	_48
	dd	86
	dd	1
	align	4
_177:
	dd	_48
	dd	87
	dd	1
	align	4
_181:
	dd	_48
	dd	88
	dd	1
	align	4
_185:
	dd	_48
	dd	89
	dd	1
	align	4
_189:
	dd	_48
	dd	90
	dd	1
	align	4
_193:
	dd	_48
	dd	91
	dd	1
	align	4
_197:
	dd	_48
	dd	92
	dd	1
	align	4
_201:
	dd	_48
	dd	93
	dd	1
	align	4
_205:
	dd	_48
	dd	94
	dd	1
	align	4
_209:
	dd	_48
	dd	95
	dd	1
	align	4
_213:
	dd	_48
	dd	96
	dd	1
	align	4
_217:
	dd	_48
	dd	97
	dd	1
	align	4
_221:
	dd	_48
	dd	98
	dd	1
	align	4
_225:
	dd	_48
	dd	99
	dd	1
	align	4
_229:
	dd	_48
	dd	100
	dd	1
	align	4
_233:
	dd	_48
	dd	101
	dd	1
	align	4
_237:
	dd	_48
	dd	102
	dd	1
	align	4
_241:
	dd	_48
	dd	103
	dd	1
	align	4
_245:
	dd	_48
	dd	104
	dd	1
	align	4
_249:
	dd	_48
	dd	105
	dd	1
	align	4
_253:
	dd	_48
	dd	106
	dd	1
	align	4
_257:
	dd	_48
	dd	107
	dd	1
	align	4
_261:
	dd	_48
	dd	108
	dd	1
	align	4
_265:
	dd	_48
	dd	109
	dd	1
	align	4
_269:
	dd	_48
	dd	110
	dd	1
	align	4
_273:
	dd	_48
	dd	111
	dd	1
	align	4
_277:
	dd	_48
	dd	112
	dd	1
	align	4
_281:
	dd	_48
	dd	113
	dd	1
	align	4
_285:
	dd	_48
	dd	114
	dd	1
	align	4
_289:
	dd	_48
	dd	115
	dd	1
	align	4
_293:
	dd	_48
	dd	117
	dd	1
	align	4
_297:
	dd	_48
	dd	118
	dd	1
	align	4
_301:
	dd	_48
	dd	119
	dd	1
	align	4
_305:
	dd	_48
	dd	120
	dd	1
	align	4
_309:
	dd	_48
	dd	121
	dd	1
	align	4
_313:
	dd	_48
	dd	122
	dd	1
	align	4
_317:
	dd	_48
	dd	123
	dd	1
	align	4
_321:
	dd	_48
	dd	124
	dd	1
	align	4
_325:
	dd	_48
	dd	125
	dd	1
	align	4
_329:
	dd	_48
	dd	126
	dd	1
	align	4
_333:
	dd	_48
	dd	128
	dd	1
	align	4
_337:
	dd	_48
	dd	129
	dd	1
	align	4
_341:
	dd	_48
	dd	130
	dd	1
	align	4
_345:
	dd	_48
	dd	131
	dd	1
	align	4
_349:
	dd	_48
	dd	132
	dd	1
	align	4
_353:
	dd	_48
	dd	134
	dd	1
	align	4
_357:
	dd	_48
	dd	135
	dd	1
	align	4
_361:
	dd	_48
	dd	136
	dd	1
	align	4
_365:
	dd	_48
	dd	137
	dd	1
	align	4
_369:
	dd	_48
	dd	138
	dd	1
	align	4
_373:
	dd	_48
	dd	139
	dd	1
	align	4
_377:
	dd	_48
	dd	140
	dd	1
	align	4
_381:
	dd	_48
	dd	141
	dd	1
	align	4
_385:
	dd	_48
	dd	142
	dd	1
	align	4
_389:
	dd	_48
	dd	143
	dd	1
	align	4
_393:
	dd	_48
	dd	144
	dd	1
	align	4
_397:
	dd	_48
	dd	145
	dd	1
	align	4
_401:
	dd	_48
	dd	147
	dd	1
	align	4
_405:
	dd	_48
	dd	148
	dd	1
	align	4
_409:
	dd	_48
	dd	149
	dd	1
	align	4
_413:
	dd	_48
	dd	151
	dd	1
	align	4
_417:
	dd	_48
	dd	152
	dd	1
	align	4
_421:
	dd	_48
	dd	153
	dd	1
	align	4
_425:
	dd	_48
	dd	155
	dd	1
	align	4
_429:
	dd	_48
	dd	156
	dd	1
	align	4
_433:
	dd	_48
	dd	158
	dd	1
	align	4
_437:
	dd	_48
	dd	159
	dd	1
	align	4
_441:
	dd	_48
	dd	160
	dd	1
	align	4
_445:
	dd	_48
	dd	162
	dd	1
	align	4
_449:
	dd	_48
	dd	163
	dd	1
	align	4
_453:
	dd	_48
	dd	164
	dd	1
	align	4
_457:
	dd	_48
	dd	165
	dd	1
	align	4
_461:
	dd	_48
	dd	166
	dd	1
	align	4
_465:
	dd	_48
	dd	167
	dd	1
	align	4
_469:
	dd	_48
	dd	168
	dd	1
	align	4
_473:
	dd	_48
	dd	169
	dd	1
_579:
	db	"RequestFiles",0
_580:
	db	"text",0
_544:
	db	"$",0
_581:
	db	"exts",0
_582:
	db	"save",0
_583:
	db	"multiple",0
_584:
	db	"path",0
_585:
	db	"file",0
_586:
	db	"dir",0
_587:
	db	"ext",0
_588:
	db	"defext",0
_589:
	db	"p",0
_590:
	db	"q",0
_591:
	db	"paths",0
_592:
	db	"[]$",0
	align	4
_578:
	dd	1
	dd	_579
	dd	2
	dd	_580
	dd	_544
	dd	-4
	dd	2
	dd	_581
	dd	_544
	dd	-8
	dd	2
	dd	_582
	dd	_480
	dd	-12
	dd	2
	dd	_583
	dd	_480
	dd	-16
	dd	2
	dd	_584
	dd	_544
	dd	-20
	dd	2
	dd	_585
	dd	_544
	dd	-24
	dd	2
	dd	_586
	dd	_544
	dd	-28
	dd	2
	dd	_480
	dd	_480
	dd	-32
	dd	2
	dd	_587
	dd	_544
	dd	-36
	dd	2
	dd	_588
	dd	_480
	dd	-40
	dd	2
	dd	_589
	dd	_480
	dd	-44
	dd	2
	dd	_590
	dd	_480
	dd	-48
	dd	2
	dd	_591
	dd	_592
	dd	-52
	dd	0
	align	4
_496:
	dd	_48
	dd	362
	dd	3
	align	4
_499:
	dd	_48
	dd	364
	dd	3
	align	4
_3:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	92
	align	4
_2:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	47
	align	4
_500:
	dd	_48
	dd	366
	dd	3
	align	4
_502:
	dd	_48
	dd	367
	dd	3
	align	4
_506:
	dd	3
	dd	0
	dd	0
	align	4
_504:
	dd	_48
	dd	368
	dd	4
	align	4
_505:
	dd	_48
	dd	369
	dd	4
	align	4
_509:
	dd	3
	dd	0
	dd	0
	align	4
_508:
	dd	_48
	dd	371
	dd	4
	align	4
_510:
	dd	_48
	dd	375
	dd	3
	align	4
_515:
	dd	_48
	dd	376
	dd	3
	align	4
_4:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	46
	align	4
_516:
	dd	_48
	dd	377
	dd	3
_543:
	db	"exs",0
	align	4
_542:
	dd	3
	dd	0
	dd	2
	dd	_543
	dd	_544
	dd	-56
	dd	0
	align	4
_518:
	dd	_48
	dd	378
	dd	4
	align	4
_5:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	44
	align	4
_519:
	dd	_48
	dd	379
	dd	4
	align	4
_521:
	dd	_48
	dd	380
	dd	4
	align	4
_7:
	dd	_bbStringClass
	dd	2147483647
	dd	2
	dw	58,44
	align	4
_6:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	58
	align	4
_522:
	dd	_48
	dd	381
	dd	4
	align	4
_9:
	dd	_bbStringClass
	dd	2147483647
	dd	2
	dw	44,59
	align	4
_8:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	59
	align	4
_523:
	dd	_48
	dd	382
	dd	4
	align	4
_524:
	dd	_48
	dd	383
	dd	4
	align	4
_541:
	dd	3
	dd	0
	dd	0
	align	4
_526:
	dd	_48
	dd	384
	dd	5
	align	4
_527:
	dd	_48
	dd	385
	dd	5
	align	4
_528:
	dd	_48
	dd	386
	dd	5
	align	4
_540:
	dd	3
	dd	0
	dd	0
	align	4
_529:
	dd	_48
	dd	387
	dd	6
	align	4
_530:
	dd	_48
	dd	388
	dd	6
	align	4
_533:
	dd	3
	dd	0
	dd	0
	align	4
_532:
	dd	_48
	dd	388
	dd	13
	align	4
_534:
	dd	_48
	dd	389
	dd	6
	align	4
_538:
	dd	3
	dd	0
	dd	0
	align	4
_536:
	dd	_48
	dd	389
	dd	14
	align	4
_537:
	dd	_48
	dd	389
	dd	23
	align	4
_539:
	dd	_48
	dd	390
	dd	6
	align	4
_545:
	dd	_48
	dd	395
	dd	3
	align	4
_556:
	dd	3
	dd	0
	dd	0
	align	4
_547:
	dd	_48
	dd	396
	dd	4
	align	4
_550:
	dd	3
	dd	0
	dd	0
	align	4
_549:
	dd	_48
	dd	397
	dd	5
	align	4
_13:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	70,105,108,101,115,0,42,46
	align	4
_553:
	dd	3
	dd	0
	dd	0
	align	4
_552:
	dd	_48
	dd	399
	dd	5
	align	4
_14:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	0,42,46
	align	4
_554:
	dd	_48
	dd	401
	dd	4
	align	4
_15:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	0
	align	4
_555:
	dd	_48
	dd	402
	dd	4
	align	4
_16:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	59,42,46
	align	4
_557:
	dd	_48
	dd	406
	dd	3
	align	4
_559:
	dd	_48
	dd	409
	dd	3
	align	4
_560:
	dd	_48
	dd	412
	dd	3
	align	4
_576:
	dd	3
	dd	0
	dd	0
	align	4
_562:
	dd	_48
	dd	414
	dd	4
	align	4
_565:
	dd	_48
	dd	417
	dd	4
	align	4
_566:
	dd	_48
	dd	420
	dd	4
	align	4
_575:
	dd	3
	dd	0
	dd	0
	align	4
_569:
	dd	_48
	dd	421
	dd	5
	align	4
_577:
	dd	_48
	dd	426
	dd	3
_602:
	db	"TranslateKey",0
_603:
	db	"Code",0
	align	4
_601:
	dd	1
	dd	_602
	dd	2
	dd	_603
	dd	_480
	dd	-4
	dd	0
	align	4
_593:
	dd	_48
	dd	492
	dd	3
	align	4
_600:
	dd	3
	dd	0
	dd	0
	align	4
_597:
	dd	_48
	dd	492
	dd	50
_614:
	db	"IsKeyDown",0
	align	4
_613:
	dd	1
	dd	_614
	dd	2
	dd	_603
	dd	_480
	dd	-4
	dd	0
	align	4
_604:
	dd	_48
	dd	518
	dd	3
	align	4
_605:
	dd	_48
	dd	519
	dd	3
	align	4
_608:
	dd	3
	dd	0
	dd	0
	align	4
_607:
	dd	_48
	dd	519
	dd	15
	align	4
_609:
	dd	_48
	dd	522
	dd	3
	align	4
_612:
	dd	3
	dd	0
	dd	0
	align	4
_611:
	dd	_48
	dd	522
	dd	42
_668:
	db	"GetOsVersion",0
_669:
	db	"cacheVersion",0
	align	4
_616:
	dd	-1
	align	4
_667:
	dd	1
	dd	_668
	dd	4
	dd	_669
	dd	_480
	dd	_616
	dd	0
	align	4
_615:
	dd	_48
	dd	568
	dd	3
	align	4
_617:
	dd	_48
	dd	571
	dd	3
_664:
	db	"data",0
_665:
	db	":brl.bank.TBank",0
	align	4
_663:
	dd	3
	dd	0
	dd	2
	dd	_664
	dd	_665
	dd	-4
	dd	0
	align	4
_619:
	dd	_48
	dd	573
	dd	4
	align	4
_620:
	dd	_48
	dd	576
	dd	4
	align	4
_622:
	dd	_48
	dd	577
	dd	4
	align	4
_623:
	dd	_48
	dd	578
	dd	4
_660:
	db	"major",0
_661:
	db	"minor",0
	align	4
_659:
	dd	3
	dd	0
	dd	2
	dd	_660
	dd	_480
	dd	-8
	dd	2
	dd	_661
	dd	_480
	dd	-12
	dd	0
	align	4
_627:
	dd	_48
	dd	580
	dd	5
	align	4
_629:
	dd	_48
	dd	581
	dd	5
	align	4
_631:
	dd	_48
	dd	584
	dd	5
	align	4
_648:
	dd	3
	dd	0
	dd	0
	align	4
_636:
	dd	_48
	dd	586
	dd	7
	align	4
_643:
	dd	3
	dd	0
	dd	0
	align	4
_642:
	dd	_48
	dd	588
	dd	9
	align	4
_645:
	dd	3
	dd	0
	dd	0
	align	4
_644:
	dd	_48
	dd	590
	dd	9
	align	4
_647:
	dd	3
	dd	0
	dd	0
	align	4
_646:
	dd	_48
	dd	592
	dd	9
	align	4
_658:
	dd	3
	dd	0
	dd	0
	align	4
_649:
	dd	_48
	dd	595
	dd	7
	align	4
_655:
	dd	3
	dd	0
	dd	0
	align	4
_654:
	dd	_48
	dd	597
	dd	9
	align	4
_657:
	dd	3
	dd	0
	dd	0
	align	4
_656:
	dd	_48
	dd	599
	dd	9
	align	4
_662:
	dd	_48
	dd	603
	dd	4
	align	4
_666:
	dd	_48
	dd	607
	dd	3
_721:
	db	"GetTempDirectory",0
_722:
	db	"resetCache",0
_723:
	db	"cache",0
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_671:
	dd	_1
_724:
	db	"hasCache",0
	align	4
_673:
	dd	0
	align	4
_720:
	dd	1
	dd	_721
	dd	2
	dd	_722
	dd	_480
	dd	-4
	dd	4
	dd	_723
	dd	_544
	dd	_671
	dd	4
	dd	_724
	dd	_480
	dd	_673
	dd	0
	align	4
_670:
	dd	_48
	dd	658
	dd	3
	align	4
_672:
	dd	_48
	dd	659
	dd	3
	align	4
_674:
	dd	_48
	dd	662
	dd	3
_714:
	db	"charSize",0
_715:
	db	"chars",0
_716:
	db	"size",0
_717:
	db	"bank",0
_718:
	db	"realSize",0
	align	4
_713:
	dd	3
	dd	0
	dd	2
	dd	_714
	dd	_480
	dd	-8
	dd	2
	dd	_715
	dd	_480
	dd	-12
	dd	2
	dd	_716
	dd	_480
	dd	-16
	dd	2
	dd	_717
	dd	_665
	dd	-20
	dd	2
	dd	_718
	dd	_480
	dd	-24
	dd	0
	align	4
_678:
	dd	_48
	dd	663
	dd	4
	align	4
_679:
	dd	_48
	dd	664
	dd	4
	align	4
_680:
	dd	_48
	dd	667
	dd	4
	align	4
_682:
	dd	_48
	dd	668
	dd	4
	align	4
_684:
	dd	_48
	dd	669
	dd	4
	align	4
_686:
	dd	_48
	dd	670
	dd	4
	align	4
_688:
	dd	_48
	dd	673
	dd	4
	align	4
_692:
	dd	_48
	dd	676
	dd	4
	align	4
_702:
	dd	3
	dd	0
	dd	0
	align	4
_694:
	dd	_48
	dd	678
	dd	5
	align	4
_695:
	dd	_48
	dd	679
	dd	5
	align	4
_696:
	dd	_48
	dd	680
	dd	5
	align	4
_699:
	dd	_48
	dd	683
	dd	5
	align	4
_703:
	dd	_48
	dd	687
	dd	4
	align	4
_712:
	dd	3
	dd	0
	dd	0
	align	4
_705:
	dd	_48
	dd	688
	dd	5
	align	4
_711:
	dd	3
	dd	0
	dd	0
	align	4
_708:
	dd	_48
	dd	689
	dd	6
	align	4
_719:
	dd	_48
	dd	695
	dd	3
_732:
	db	"KeepSystemAlive",0
	align	4
_731:
	dd	1
	dd	_732
	dd	0
	align	4
_725:
	dd	_48
	dd	724
	dd	2
	align	4
_730:
	dd	3
	dd	0
	dd	0
	align	4
_727:
	dd	_48
	dd	724
	dd	12
_739:
	db	"GetMousePosition",0
_740:
	db	"point",0
	align	4
_738:
	dd	1
	dd	_739
	dd	2
	dd	_740
	dd	_493
	dd	-4
	dd	0
	align	4
_733:
	dd	_48
	dd	741
	dd	3
_734:
	db	"i",0
	align	4
_736:
	dd	_48
	dd	742
	dd	3
	align	4
_737:
	dd	_48
	dd	743
	dd	3
